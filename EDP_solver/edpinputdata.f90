module datainput
use iso_fortran_env, only: real64
    use, intrinsic :: iso_fortran_env, only : input_unit, &
                                              output_unit, &
                                              error_unit
implicit none


contains
    subroutine defvar(c1,c2,tetha,x0,xf,t0,tf,Te0,Tef,n,m,ntotales,mtotales,mallax,fx0)
        integer::n,m,ntotales,mtotales,i
        real(real64)::c1,c2,tetha,x0,xf,t0,tf,Te0,Tef
        real(real64),allocatable::mallax(:),fx0(:)

        c1=2 !coeficiente difusión
        c2=0.018 !coeficiente fuentes
        tetha=0.6 !Parametro tetha metodo 1 implicito 0 explicito
        x0=0
        xf=10
        t0=0
        tf=1000
        !numero nodos
        n=15
        m=1000
        ntotales=n+2 !contando esquinas;
        mtotales=m+1
        Te0=0.2
        Tef=0.1
        allocate(mallax(ntotales),fx0(ntotales))
        mallax = (/(((xf-x0)/(ntotales))*i,i=1,ntotales)/)
        fx0=0.1*cos(mallax*5)

    end subroutine
end module datainput
