program edpmain !main program
use iso_fortran_env, only: real64
    use, intrinsic :: iso_fortran_env, only : input_unit, &
                                              output_unit, &
                                              error_unit
use matrix_class
use datainput
use matrix_system
use qr_factorization
use iomodule

implicit none

integer::n,m,ntotales,mtotales
real(real64)::c1,c2,tetha,x0,xf,t0,tf,Te0,Tef,k,h,f(3)
type(matrix)::B,Al,C,A1,A2,Q,R
real(real64),allocatable::arraynohom(:),fx0(:),mallax(:),T(:),bs(:),Tlast(:)
integer::ti
!array de construccion de B
f=(/1,-2,1/)
!Lectura de parametros y condiciones iniciales, modularizada para hacer la fuente independiente del programa principal
call defvar(c1,c2,tetha,x0,xf,t0,tf,Te0,Tef,n,m,ntotales,mtotales,mallax,fx0)
!Creacion de la matriz del sistema homogeno sin fuentes
call mat_heat_B(f,ntotales,B)


! Calculo pasos
k=(tf-t0)/(m+1)
h=(xf-x0)/(n+1)
!creacion del termino no homogeneo
call mat_heat_nonhom(k,c2,tetha,ntotales,arraynohom)
!creacion de una matriz diagonal ntotalesxntotales
call mat_heat_Al(ntotales,Al)
!creacion de una matriz diagonal ntotalesxntotales
call mat_heat_C(ntotales,C)
!construccion de las matrices del sistema A1Uj=A2Uj-1 +nohom
call mat_heat_system(A1,A2,k,tetha,c1,h,B,c2,Al,C)

!Imposicion de las condiciones de frontera, tipo Dirichlet en este caso, temperatura fija en los extremos, El valor de la temperatura vendra dado en el array de temperatura, en los nodos de la esquina en la matriz el coeficiente sera 1
call set(A1,1,1,1._real64)
call set(A1,ntotales,ntotales,1._real64)
call set(A2,1,1,1._real64)
call set(A2,ntotales,ntotales,1._real64)






!Resolucion del sistema, alojamiento del array de temperatura para j y j-1 asi como para el array de datos de Ax=b
allocate(T(ntotales),bs(ntotales),Tlast(ntotales))
!Calculo de U0 como A2T_0
Tlast=matmul(A2,fx0)
Tlast(1)=Te0
Tlast(size(Tlast))=Tef
!Descomposicion qr de A1, la gran ventaja computacional respecto a calcular la inversa de la matriz por eliminacion gaussiana es que solo se implementa una vez para todo el bucle
call qr(A1,Q,R)

do ti=1,mtotales
    bs=matmul(A2,Tlast)+arraynohom
    call traspose_square(Q)
!resolucion del sistema RUj=trasp(Q)A2Uj-1+trasp(Q)Terminonohomogeneo por remonte al ser R triangular superior
!Metodo remonte y traspose implementado en matrix_class
    T=remonte(R,matmul(Q,bs))
    call traspose_square(Q)
    Tlast=T
!Imposicion de las condiciones de contorno en Uj
   Tlast(1)=Te0
    Tlast(size(Tlast))=Tef

end do
!Salida de datos
call T_output(T)



end program
