module matrix_system
use iso_fortran_env, only: real64
    use, intrinsic :: iso_fortran_env, only : input_unit, &
                                              output_unit, &
                                              error_unit
use matrix_class
implicit none

!Sistema matricial (I +kB+k2C)Uj=(I+k3B+k4C)Uj-1 + terminonohomogeneo
!I,C matriz diagonal, C matriz término fuentes
! B matriz de la ecuacion de difusion sin fuentes

contains
!subrutina para crear la matriz tridiagonal B, se crea desplazando el array f por la diagonal de la matriz

subroutine mat_heat_B(f,ntotales,B)
    type(matrix),intent(out)::B
    real(real64),intent(in)::f(3)
    integer,intent(in)::ntotales
    integer::j,m
     do j=1,(ntotales-(size(f)-1))
     do m=1,size(f)
        call set(B,j+1,j-1+m,f(m))
     end do
     end do
     call set(B,ntotales,ntotales,0._real64)
end subroutine

!Termino  no homogeneo
subroutine mat_heat_nonhom(k,c2,tetha,ntotales,arraynohom)
real(real64),allocatable,intent(out)::arraynohom(:)
real(real64),intent(in)::k,c2,tetha
integer,intent(in)::ntotales
integer::i
allocate(arraynohom(ntotales))
do i=1,ntotales
arraynohom(i)=k*c2*(2*tetha-1)
end do
end subroutine
!Matriz diagonal
subroutine mat_heat_Al(ntotales,Al)
    type(matrix),intent(out)::Al
    integer,intent(in)::ntotales
    integer::j
     do j=1,ntotales
       call set (Al,j,j,1._real64)
     end do
end subroutine
!subrutina de matriz diagonal redundante (explicitada por motivos de claridad)
subroutine mat_heat_C(ntotales,C)
type(matrix),intent(out)::C
    integer,intent(in)::ntotales
    integer::j
     do j=1,ntotales
       call set (C,j,j,1._real64)
     end do
end subroutine

!Generacion de las matrices finales del sistema
!A1=Al-(k*tetha*c1/h^2)*B-k*c2*tetha*C;
!A2=Al+(k*(1-tetha)*c1/h^2)*B+k*c2*(1-tetha)*C
subroutine mat_heat_system(A1,A2,k,tetha,c1,h,B,c2,Al,C)
type(matrix),intent(in)::B,Al,C
type(matrix),intent(out)::A1,A2
type(matrix)::Aux1,Aux2,Aux3
real(real64),intent(in)::k,tetha,c1,h,c2

!La creación de matrices auxiliares alojando memoria es muy lenta, pero al solo necesitar construirse una vez la matriz
!es asumible, por el mismo motivo las operaciones de multiplicacion de matrices con type(matrix) son muy costosas mientras que aquellas que mantienen elementos fijos son aceptables
Aux1=matmul(B,-k*tetha*c1/h**2)
Aux2=matmul(C,(-k*c2*tetha))
Aux3=matrix_add(Aux1,Aux2)
A1=matrix_add(Al,Aux3)

Aux1=matmul(B,(k*(1-tetha)*c1/h**2))
Aux2=matmul(C,(k*c2*(1-tetha)))
Aux3=matrix_add(Aux1,Aux2)
A2=matrix_add(Al,Aux3)

call dealloc(Aux1)
call dealloc(Aux2)
call dealloc(Aux3)

end subroutine


end module matrix_system
