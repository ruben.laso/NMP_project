module iomodule
use iso_fortran_env, only: real64
    use, intrinsic :: iso_fortran_env, only : input_unit, &
                                              output_unit, &
                                              error_unit
implicit none

!descomentar la linea de abajo para volcar la salida a un fichero de texto
!integer,parameter::o_unit=20 instead of stdoutput


contains
!Se redirige la salida a stdout para poder utilizarse el programa compilado y acceder a su salida desde un script de python
subroutine T_output(T)

    real(real64),allocatable,intent(in)::T(:)
    integer::i,n
    !descomentar la linea de abajo para volcar la salida a un fichero de texto
    !open(unit=o_unit,file="T_results.txt",action="write",status="replace")
    n=size(T)
    do i=1,n
    write(*,*)T(i)
    end do
    !descomentar la linea de abajo para volcar la salida a un fichero de texto
    !close(unit=o_unit)	
    end subroutine

end module

