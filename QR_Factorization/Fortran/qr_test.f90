program qr_test
    use iso_Fortran_env, only: real64
    use, intrinsic :: iso_fortran_env, only : input_unit, &
                                              output_unit, &
                                              error_unit
    use matrix_class
    use matrix_io_module
    use qr_factorization

    implicit none

    type(matrix) :: A, Q, R
    character(255) :: file
    integer :: err
    integer :: nargs

    ! print *, 'Program for testing QR factorization'
    ! print *, ' /  0  0 -1 -2 \ '
    ! print *, ' |  0  0  0  1 | '
    ! print *, ' |  0  1  2  3 | '
    ! print *, ' \ -1 -2 -3 -4 / '
    ! print *, ' '

    ! print *, 'Program for testing QR factorization'
    ! print *, ' /  12 -51   4  \ '
    ! print *, ' |  4   167 -68 | '
    ! print *, ' \ -4   24  -41 / '
    ! print *, ' '

    ! call set(a,1,1,9._real64); call set(a,1,2,2._real64); call set(a,1,3,1._real64);
    ! call set(a,2,1,4._real64); call set(a,2,2,9._real64); call set(a,2,3,4._real64);
    ! call set(a,3,1,7._real64); call set(a,3,2,8._real64); call set(a,3,3,9._real64);

    ! call alloc(a, [1,1,1, 2,2,2, 3,3,3], &
    !               [1,2,3, 1,2,3, 1,2,3], &
    !               real([12,-51,4, 6,167,-68, -4,24,-41], real64))

    ! call alloc(a, [1,1,1,1, 2,2,2,2, 3,3,3,3, 4,4,4,4], &
    !               [1,2,3,4, 1,2,3,4, 1,2,3,4, 1,2,3,4], &
    !               real([0,0,-1,-2, 0,0,0,1, 0,1,2,3, -1,-2,-3,-4], real64))


    ! call alloc(a, [1,1,1, 2,2,2, 3,3,3], &
    !               [1,2,3, 1,2,3, 1,2,3], &
    !               real([0,1,1, 1,1,2, 0,0,3], real64))

    nargs = iargc()
    if ( nargs < 1 ) then
        write(error_unit, *) "Use this program executing it like:"
        write(error_unit, *) "$ ./program.out matrix_A.txt matrix_Q.txt matrix_R.txt"
        write(error_unit, *) "matrix_A.txt : is the file to read the matrix to factorize"
        write(error_unit, *) "matrix_Q.txt matrix_R.txt : are optional args to write the factorization result in specified files"
    end if


    call get_command_argument(1, value = file, status = err)
    if ( err /= 0 ) then
        stop 'Error getting the argument 1. Exiting'
    end if
    print *, 'Reading matrix A from file: ', file
    call read_from_file(A,file)
    ! call read_from_file_Matrix_Market(A,file)

    ! Print A matrix, and compute A=Q*R
    print*, 'A = '
    call show(A)
    print *, ''
    call qr(A,Q,R)

    print*, 'Q = '
    call show(Q)
    print *, ''

    print*, 'R = '
    call show(R)
    print *, ''


    print *, 'Checking A=Q*R...'
    call show(matrix_multiplication(Q,R))
    print *, ''

    if (nargs > 2) then
        call get_command_argument(2, value = file, status = err)
        if ( err /= 0 ) then
            stop 'Error getting the argument 2. Exiting'
        end if
        print *, 'Writing matrix Q in file: ', file
        call write_to_file(Q,file)

        call get_command_argument(3, value = file, status = err)
        if ( err /= 0 ) then
            stop 'Error getting the argument 3. Exiting'
        end if
        print *, 'Writing matrix R in file: ', file
        call write_to_file(R,file)
    end if

    ! Free memory used
    call dealloc(A)
    call dealloc(Q)
    call dealloc(R)
end program qr_test
