! Module for helping in the Input/Output operations with matrices
module matrix_io_module
    use iso_fortran_env, only: real64
    use, intrinsic :: iso_fortran_env, only : input_unit, &
                                              output_unit, &
                                              error_unit
    use matrix_class

    implicit none
    contains

    ! Fills a matrix with the content of a file
    subroutine read_from_file(A, file)
        type(matrix), intent(inout) :: A
        character(255), intent(in) :: file
        real(real64), allocatable :: values(:)
        integer :: err
        integer :: i, j, rows, cols
        integer :: file_unit

        file_unit = 100;

        open(unit=file_unit, file=file, iostat=err, action="read")
        if ( err /= 0 ) then
            write(error_unit, *) "Error opening file ", file
            call exit
        end if

        read(file_unit, *, iostat=err) rows, cols
        if ( err /= 0 ) then
            stop "Read error in file unit"
            call exit
        end if

        allocate(values(cols), stat=err)
        if (err /= 0) write(error_unit, *) "values(cols): Allocation request denied"

        do i = 1, rows, 1
            read(file_unit, *, iostat=err) values
            if ( err /= 0 ) then
                stop "Read error in file unit "
                call exit
            end if

            do j = 1, cols, 1
                call set(A, i, j, values(j))
            end do
        end do

        close(unit=file_unit, iostat=err)
        if ( err /= 0 ) write(error_unit, *) "Error closing file unit ", file_unit

        if (allocated(values)) deallocate(values, stat=err)
        if (err /= 0) write(error_unit, *) "values(cols): Deallocation request denied"
    end subroutine read_from_file


    subroutine write_to_file(A, file)
        type(matrix), intent(in) :: A
        character(255) :: file
        integer :: err
        integer :: i, j, rows, cols
        integer :: file_unit

        file_unit = 100;

        open(unit=file_unit, file=file, iostat=err, action="write")
        if ( err /= 0 ) then
            write(error_unit, *) "Error opening file ", file
            call exit
        end if

        rows = size(A, 1)
        cols = size(A, 2)

        write(file_unit, *) rows, cols
        do i = 1, rows, 1
            write(file_unit, *) get(A, i, [(j, j=1, cols)])
        end do

        close(unit=file_unit, iostat=err)
        if ( err /= 0 ) stop "Error closing file unit file_unit"

    end subroutine write_to_file
end module matrix_io_module
