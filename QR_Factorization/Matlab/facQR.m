function [ Q, R] = facQR( A )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
[m,n]=size(A);
%s=zeros(1,n);
Q=eye(m);
for i=1:n-1;
    %com hace referencia al primer elemento de la columna que se va a
    %seleccionar. La progresi�n deber�a ser de (1,1)(2,2)(3,3)...
    H=eye(m);
    Q1=eye(m);
    % com=A(i,i);
    %Selecciono la columna y hago la norma
    v=A(i:m,i);
    no=norm(v);
    fill=zeros(1,m-i+1);
    fill(1)=no;
    dif=v'-fill;
    no2=norm(dif);
    v2=dif/no2;
    I=eye(m-i+1);
    H(i:m,i:m)=(I-(2*v2'*v2)./(v2*v2')); % sustituir v2 por diff
    Q1(i:m,i:m)=(I-(2*v2'*v2)./(v2*v2'));
    Q=Q*Q1;
    R=(H*A);
    A=R;
    %k=eye(m)-(2*v2'*v2)./(v2*v2');
    %H()=H1
    %H1=H.*H1;
    %H=H1;
end

end
