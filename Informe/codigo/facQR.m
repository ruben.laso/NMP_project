function [Q, R] = facQR(A)
[m,n]=size(A);
Q=eye(m);
for i=1:n-1;
    H=eye(m);
    Q1=eye(m);
    v=A(i:m,i);
    no=norm(v);
    fill=zeros(1,m-i+1);
    fill(1)=no;
    dif=v'-fill;
    no2=norm(dif);
    v2=dif/no2;
    I=eye(m-i+1);
    H(i:m,i:m)=(I-(2*v2'*v2)./(v2*v2'));
    Q1(i:m,i:m)=(I-(2*v2'*v2)./(v2*v2'));
    Q=Q*Q1;
    R=(H*A);
    A=R;
end
end
