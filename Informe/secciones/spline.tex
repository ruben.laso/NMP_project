\section{El experimento de Franck--Hertz}
El experimento de Franck--Hertz demuestra la cuantización de los niveles energéticos de los electrones en un átomo. En su momento confirmó la teoría atómica de Bohr, demostrando que los átomos solo pueden absorber cantidades cuantizadas de energía, constituyendo así uno de los experimentos fundamentales de la física cuántica.

En esta experiencia, los electrones son acelerados a través de una diferencia de potencial ($V_{ac}$) en un tubo de vacío lleno de gas, en nuestro caso neon. Se produce una corriente eléctrica, cuya intensidad ($I_{amp}$) es medida en un amplificador, y a partir de estos valores se construye la curva de corriente característica. A continuación se determinan los mínimos y máximos de esta curva, y a partir de ellos se realiza un ajuste lineal del orden de aparición de cada extremo con su correspondiente voltaje. Es decir, el primer mínimo corresponde con $n=1$, el segundo con $n=2$ y el tercero con $n=3$, e igualmente para los máximos, haciéndose separadamente los ajustes para mínimos y máximos. A partir de este análisis se puede obtener el salto energético entre el estado fundamental y el primer estado excitado del neon.  

La curva de corriente\footnote{Los datos experimentales empleados fueron tomados durante las prácticas de laboratorio de Física Cuántica, correspondientes a la asignatura \emph{Técnicas Experimentais III} del Grado en Física de la USC.} se muestra en la figura~\ref{fig:puntos-corriente}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\columnwidth]{figuras/FHPlotNoSplines.eps}
	\caption{Curva de corriente (puntos experimentales). La intensidad de corriente se mide en nanoamperios y la diferencia de potencial en voltios.}
	\label{fig:puntos-corriente}
\end{figure}

Se observa que no hay demasiados puntos experimentales en el entorno de los extremos de la curva de corriente, por lo que se hace complicado determinar con precisión los máximos y mínimos simplemente observando la gráfica. Podemos resolver este problema llevando a cabo una interpolación de los datos, empleando \textit{splines} cúbicos, para posteriormente determinar los extremos a partir de la interpolación.

\subsection{Utilización de \textit{splines} cúbicos}
Se programa una función de MATLAB llamada \texttt{CubicSpline.m} (ver código~\ref{lst:cubic-splines} del apéndice~\ref{ap:codigo-fuente}), que interpolaría un conjunto de datos genérico dado. Esta función tiene como entrada los pares de puntos a interpolar, en este caso voltaje e intensidad. Como salida tenemos una matriz que incluye el polinomio cúbico de interpolación en cada intervalo; además, a partir del polinomio, calculamos diez pares de puntos en cada intervalo, de cara a dibujar nuestra solución gráficamente. La construcción del código se encuentra pormenorizada en el mismo.

Centrándonos en nuestro problema, creamos el \textit{script} \texttt{FH.m} (ver código~\ref{lst:FH} del apéndice~\ref{ap:codigo-fuente}), que lee los valores (\texttt{V}, \texttt{I}) y llama a la función \texttt{CubicSpline.m}. Ésta nos reporta el \textit{spline} interpolador en cada intervalo y grafica la solución. Finalmente, se utilizan diversos comandos propios de polinomios, aplicados a los \textit{splines} solución de nuestro problema, para hallar los extremos de la curva de corriente como se detalla en el programa. 

Para ejecutar el programa basta con escribir \texttt{FH} en la ventana de comandos de MATLAB.

\subsection{Solución del problema}
Una vez interpolamos los datos, obtenemos la curva de corriente reconstruida que se muestra en la figura~\ref{fig:interpolacion}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\columnwidth]{figuras/FHPlotSplines.eps}
	\caption{Curva de corriente con los puntos experimentales (en azul) interpolados con \textit{splines} cúbicos. Los máximos y mínimos aparecen marcados en rojo.}
	\label{fig:interpolacion}
\end{figure}

En la gráfica anterior podemos observar los extremos de la curva de corriente, que se detallan en la tabla~\ref{tab:extremos}.

\begin{table}[htbp]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		$V_{ac}^{\min} \ (\mathrm{V})$ & $V_{ac}^{\max}  \ (\mathrm{V})$ \\ \hline\hline
		           $21.43$             &             $16.17$             \\ \hline
		           $40.31$             &             $33.82$             \\ \hline
		           $59.11$             &             $52.34$             \\ \hline
	\end{tabular}
	\caption{Extremos de $V_{ac}$.}
	\label{tab:extremos}
\end{table}

Sin entrar en cálculo de incertidumbres, veamos en la figura~\ref{fig:ajuste-lineal} el ajuste lineal llevado a cabo para éstos mínimos y máximos:
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\columnwidth]{figuras/FHLinearFit.eps}
	\caption{Ajuste lineal para los máximos (magenta) y los mínimos (azul). Representamos el orden de aparición del extremo ($n$) y la diferencia de potencial ($V_{ac}$).}
	\label{fig:ajuste-lineal}
\end{figure}

Se puede deducir que la pendiente de cada uno de los ajustes debería coincidir con el salto energético (en electronvoltios) entre el estado fundamental y el primer excitado del neon. Cabe esperar el mismo resultado para mínimos y máximos, y si promediamos  resulta un salto energético de $\Delta E \approx 18 \ \mathrm{eV}$. Experimentos mucho más precisos reportan un valor similar, con lo cual el procedimiento seguido es más que aceptable.

En definitiva, si bien hemos tomado escasos puntos experimentales en el laboratorio, o si nuestros valores no están próximos a los extremos, la opción de la interpolación con \textit{splines} cúbicos es una buena solución a nuestro problema.
