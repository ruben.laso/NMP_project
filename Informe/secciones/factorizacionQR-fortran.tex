\subsection{Implementación en Fortran}
Para la implementación de la factorización QR en el lenguaje de programación Fortran (siguiendo los estándares de Fortran90), se ha utilizado como guía el programa de MATLAB mostrado en el código~\ref{lst:QR-matlab}.

\lstinputlisting[language=MATLAB, caption={Código de la factorización QR en MATLAB.}, label={lst:QR-matlab}]{codigo/facQR.m}

% TODO explicar código MATLAB

En previsión de que el tamaño de las matrices aquí manejadas iba a ser considerable, y que la matriz $R$ iba a ser diagonal superior, se ha considerado el uso de la clase \texttt{matrix\_class} diseñada en las propias prácticas de la materia.
Como se debe recordar, esta clase utilizaba una estructura COO (\textit{Coordinate lists}) para almacenar los elementos de la matriz, de esta manera se ahorraba una gran cantidad de memoria en el almacenamiento de matrices dispersas (como es el caso de $R$).
También se debe mencionar que el tener una clase para el almacenamiento de matrices nos permite definir ciertos métodos (\texttt{eye()}, \texttt{zeros()}, etc.) para hacer el código Fortran más similar al de MATLAB, facilitando así la ``traducción'' del código.

De esta manera, los cambios más significativos realizados en la clase \texttt{matrix\_class} realizada en las prácticas son:
\begin{itemize}
	\item Definición de nuevas subrutinas:
		\begin{itemize}
			\item \texttt{eye(this, n)}: dados un entero \texttt{m} y una matriz \texttt{this}, se genera la matriz identidad de orden $n$ tal que $\texttt{this} = I_n$.
			
			\item \texttt{zeros(this, n)}: dados un entero \texttt{n} y una matrix \texttt{this}, se define el tamaño de la matriz \texttt{this} tal que $\texttt{this} \in \mathcal{M}_{n \times n}$. Este método es necesario para tener definidas correctamente las dimensiones de las matrices para su posterior multiplicación.
			
			\item \texttt{copy\_matrix(src, dst)}: se copia el contenido de la matrix \texttt{src} en \texttt{dst}.
			
			\item \texttt{traspose\_square(this)}: transpone la matriz cuadrada \texttt{this}.
		\end{itemize}
	\item Definición de nuevas funciones:
		\begin{itemize}
			\item \texttt{mat\_scalar\_prod(this, x)}: dada una matriz \texttt{this}, devuelve la matriz resultado de multiplicar todos los elementos de \texttt{this} por el número real \texttt{x}.
			
			\item \texttt{mat\_scalar\_div(this, x)}: dada una matriz \texttt{this}, devuelve la matriz resultado de dividir todos los elementos de \texttt{this} por el número real \texttt{x}.
			
			\item \texttt{matrix\_add(a, b)}: dadas dos matrices \texttt{a} y \texttt{b}, devuelve la matriz resultado de sumar \texttt{a} y \texttt{b}.
			
			\item \texttt{matrix\_sub(a, b)}: dadas dos matrices \texttt{a} y \texttt{b}, devuelve la matriz resultado de restar \texttt{a} y \texttt{b}.
			
			\item \texttt{remonte(A, bs)}: resuelve utilizando la técnica de \emph{remonte} o \emph{sustitución hacia atrás} el sistema $Ax=b$.
		\end{itemize}
	\item Modificación de métodos previos:
		\begin{itemize}
			\item \texttt{set\_sca(this, i, j, val)}: en este método se ha añadido una comprobación para no introducir valores en la matriz cercanos al épsilon de la máquina\footnote{En este caso consideramos despreciables números menores que $10^{-15}$.}, evitando así posibles errores numéricos debido a estos valores y ahorrando memoria cuando se pretenden almacenar ceros o valores muy cercanos a cero (lo cual sería innecesario).
			
			\item \texttt{get\_sca(this, i, j)}: se ha añadido una condición \textit{if} para comprobar si los \textit{arrays} \texttt{row}, \texttt{col} y \texttt{val} están alojados. Esta modificación viene del hecho en que, al realizar primero una llamada a \texttt{dealloc(array)} y después a \texttt{size(array)}, \texttt{size()} nos devuelve el tamaño de \texttt{array} antes de haber sido desalojado, lo que conlleva inconsistencias y puede provocar lecturas de posiciones de memoria no reservadas (con sus consecuentes \textit{segmentation fault}).
			
			\item \texttt{dealloc()}: se ha añadido un \textit{reset} del valor de las variables $n$ y $m$ a cero. Esto resulta útil cuando se reutiliza la variable para almacenar otra matriz. Volviendo a asignar $\texttt{n}=\texttt{m}=0$ evitamos que se devuelvan tamaños de matrices incorrectos.
		\end{itemize}
\end{itemize}

Con estas funciones, y las ya existentes en la clase \texttt{matrix\_class}, se ha construido el código~\ref{lst:QR-fortran} contenido por completo en el apéndice~\ref{ap:codigo-fuente}. Debido al tamaño de este código, en esta sección solo se explicarán las partes más destacadas del mismo y las que difieran más con el código de MATLAB.

En primer lugar se importan el módulo relativo a la clase \texttt{matrix\_class}, y las partes del módulo \texttt{iso\_fortran\_env} para el uso de datos \texttt{real64} y las macros para la entrada/salida estándar y salida de errores estándar, para informar debidamente al usuario de los posibles errores que se produzcan durante la ejecución.

\lstinputlisting[language=Fortran, firstline=0, lastline=7]{codigo/qr_factorization.f90}

Debido a que la función contiene dos salidas, las matrices $Q$ y $R$, se debe definir el método como una subrutina, con los \textit{intents} correctamente definidos, de entrada para \texttt{A}, y de salida para \texttt{Q} y \texttt{R}.
También en la declaración de las variables utilizadas dentro del programa, cabe destacar que \texttt{diff} y \texttt{diff\_t} son definidas como matrices para facilitar el producto que se realizará en el algoritmo.

\lstinputlisting[language=Fortran, firstline=12, lastline=23]{codigo/qr_factorization.f90}

El cálculo de $I_{m - i + 1} - \frac{2}{v v^t} v^t v$ se realiza en varios pasos, utilizando la variable \texttt{aux\_matrix}.

\lstinputlisting[language=Fortran, firstline=71, lastline=83]{codigo/qr_factorization.f90}

Al finalizar cada una de las iteraciones del lazo del algoritmo, es necesario liberar la memoria reservada, para evitar reciclar valores indeseados en la siguiente iteración, además de ahorrar memoria (en cada iteración se necesitan vectores más pequeños).
También de esta forma nos aseguramos de que al finalizar la factorización, hemos liberado toda la memoria reservada, evitando \textit{memory leaks} en nuestro programa.

\lstinputlisting[language=Fortran, firstline=94, lastline=113]{codigo/qr_factorization.f90}

Es necesario mencionar, que en la primera implementación de este algoritmo, todas las matrices auxiliares empleadas eran del tipo \texttt{matrix\_class}. Sin embargo, operar con las estructuras de almacenamiento COO provocaba enormes problemas de rendimiento (ya en matrices de orden 8 la factorización empleaba más de 5 minutos en llevarse a cabo).
Esto se debía a la gran cantidad de llamadas a \texttt{move\_alloc} dentro de la función \texttt{set}\footnote{Las funciones de realojamiento de memoria son muy costosas, pues se debe realizar una nueva reserva, una copia de los elementos a la nueva dirección y, por último, la liberación del antiguo espacio.} y al prácticamente nulo provecho que se podía sacar de la localidad de los datos dentro de los diferentes niveles de la jerarquía de memoria debido a la estructura de datos COO, donde elementos contiguos de la matriz no se guardan en posiciones contiguas de la memoria\footnote{Esto es crítico en el rendimiento de un programa, pues al acceder a datos en posiciones contiguas de memoria podemos aprovechar que dichos datos están habitualmente dentro de la misma línea caché, que estará ya cargada dentro de la memoria caché del computador (en el mejor de los casos estará en la L1d), por lo que su tiempo de acceso es mucho menor en comparación a realizar la transacción de esa línea desde la memoria principal.}.
A mayores, la complejidad computacional de realizar una operación \texttt{get()} en una matriz de orden $n$ con una lista COO es de $\mathcal{O}(n^2)$, mientras que en un vector multidimensional es de $\mathcal{O}(\text{cte})$, lo que hace aún más ineficiente el operar de forma generalizada con estas estructuras.

Así pues, se ha optado por implementar estas matrices auxiliares como vectores multidimensionales de Fortran. De esta manera, además de facilitar la implementación, se pueden utilizar las funciones propias del lenguaje (que se suponen completamente optimizadas) disminuyendo los tiempos de ejecución enormemente (en matrices de orden 8, se obtiene el resultado en milésimas de segundo).